//
//  NetworkManager.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

class NetworkManager {
    
    static let shared: NetworkManager = NetworkManager()
    
    func dataTask(serviceURL: String, httpMethod: HttpMethod, parameters: parametersBody?, completion:@escaping (responseData) -> Void) -> Void {
        requestResource(serviceURL: serviceURL, httpMethod: httpMethod, parameters: parameters, completion: completion)
    }
    
    func dataGetTask(serviceURL: String, httpMethod: HttpMethod, completion:@escaping (responseData) -> Void) -> Void {
        requestGetResource(serviceURL: serviceURL, httpMethod: httpMethod, completion: completion)
    }
    
    private func requestResource(serviceURL: String, httpMethod: HttpMethod, parameters: parametersBody?, completion: @escaping (responseData) -> Void) -> Void {
  
        var request: URLRequest = URLRequest(url: URL(string:"\(BaseURL.data)\(serviceURL)")!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.rawValue
        
        if (parameters != nil) {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted)
        }
        
        let sessionTask: URLSessionDataTask = URLSession(configuration: .default).dataTask(with: request) { (data, response, error) in
            if (data != nil){
                let result = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                completion ((data, result as AnyObject, nil))
            }
                
            if (error != nil) {
                completion ((nil, nil, error!))
            }
        }
        sessionTask.resume()
    }
    
    private func requestGetResource(serviceURL: String, httpMethod: HttpMethod, completion: @escaping (responseData) -> Void) -> Void {
        var request: URLRequest = URLRequest(url: URL(string:"\(BaseURL.data)\(serviceURL)")!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.rawValue
        let sessionTask: URLSessionDataTask = URLSession(configuration: .default).dataTask(with: request) { (data, response, error) in
            if (data != nil){
                let result = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                completion ((data, result as AnyObject, nil))
            }
            if (error != nil) {
                completion ((nil, nil, error!))
            }
        }
        sessionTask.resume()
    }
}
