//
//  ViewController.swift
//  examen-ios
//
//  Created by User on 15/09/22.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let listMovieViewController: ListMovieViewController = ListMovieRouter.createListMovieViewController()
        self.navigationController?.pushViewController(listMovieViewController, animated: true)
    }
}

