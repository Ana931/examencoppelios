//
//  ListMovieViewController.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import UIKit

class ListMovieViewController: UIViewController {
    var presenter: ListMoviePresenterProtocol?
    private var results: [MovieModels] = []
    
    private lazy var popularButton: UIButton = {
        let button: UIButton = UIButton()
        button.backgroundColor = UIColor.init(named: Colors.Blue300)
        button.setTitle("Popular", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        return button
    }()
    private lazy var topRatedButton: UIButton = {
        let button: UIButton = UIButton()
        button.backgroundColor = UIColor.init(named: Colors.Blue300)
        button.setTitle("Top Rated", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        return button
    }()
    private lazy var onTvButton: UIButton = {
        let button: UIButton = UIButton()
        button.backgroundColor = UIColor.init(named: Colors.Blue300)
        button.setTitle("On Tv", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        return button
    }()
    private lazy var airingTodayButton: UIButton = {
        let button: UIButton = UIButton()
        button.backgroundColor = UIColor.init(named: Colors.Blue300)
        button.setTitle("Airing Today", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        button.setTitleColor(UIColor.white, for: .normal)
        return button
    }()
    private lazy var buttonsStackView: UIStackView = {
        let stackView: UIStackView = UIStackView()
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        return stackView
    }()
    private lazy var listMovieColletionView: UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.backgroundColor = UIColor.black
        return collectionView
    }()
    private enum LayoutConstant {
        static let spacing: CGFloat = 8.0
        static let itemHeight: CGFloat = 250.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        addSubViews()
        addConstraints()
        presenter?.viewDidLoad()
        configureListMovieColletionView()
    }
    
    private func configureView() {
        view.backgroundColor = UIColor.white
    }
    
    private func addSubViews() {
        buttonsStackView.addArrangedSubview(popularButton)
        buttonsStackView.addArrangedSubview(topRatedButton)
        buttonsStackView.addArrangedSubview(onTvButton)
        buttonsStackView.addArrangedSubview(airingTodayButton)
        view.addSubview(buttonsStackView)
        view.addSubview(listMovieColletionView)
    }
    
    private func configureListMovieColletionView() {
        self.listMovieColletionView.register(MovieLittleDescriptionCell.self, forCellWithReuseIdentifier: "MovieLittleDescriptionCell")
        self.listMovieColletionView.dataSource = self
        self.listMovieColletionView.delegate = self
        self.listMovieColletionView.backgroundColor = .white
        self.listMovieColletionView.alwaysBounceVertical = true
    }
    
    private func addConstraints() {
        addConstraintForButtonsStackView()
        addConstraintForListMovieColletionView()
    }
    
    private func addConstraintForButtonsStackView() {
        buttonsStackView.setAnchors(topAnchor: view.safeAreaLayoutGuide.topAnchor, leadingAnchor: view.leadingAnchor, trailingAnchor: view.trailingAnchor, topConstant: 10, leadingConstant: 16, trailingConstant: -16)
        buttonsStackView.setHeight(heightConstant: 50)
    }
    
    private func addConstraintForListMovieColletionView() {
        listMovieColletionView.setAnchors(topAnchor: buttonsStackView.bottomAnchor, bottomAnchor: view.bottomAnchor, leadingAnchor: view.leadingAnchor, trailingAnchor: view.trailingAnchor, topConstant: 18, leadingConstant: 16, trailingConstant: -16)
    }
}

extension ListMovieViewController: ListMoviePresenterToViewProtocol {
    func listMovieSuccess(listMovie: ListMovieModels) {
        results = listMovie.results
        DispatchQueue.main.async { [weak self] in
            self?.listMovieColletionView.reloadData()
        }
    }
    
    func listMovieFailure() {
        
    }
}

extension ListMovieViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: MovieLittleDescriptionCell = listMovieColletionView.dequeueReusableCell(withReuseIdentifier: "MovieLittleDescriptionCell", for: indexPath) as? MovieLittleDescriptionCell else {
            return UICollectionViewCell()
        }
        cell.dataContent(with: results[indexPath.row])
        return cell
    }
}

extension ListMovieViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = itemWidth(for: view.frame.width, spacing: 0)
        return CGSize(width: width, height: LayoutConstant.itemHeight)
    }

    func itemWidth(for width: CGFloat, spacing: CGFloat) -> CGFloat {
        let itemsInRow: CGFloat = 2
        let totalSpacing: CGFloat = 2 * spacing + (itemsInRow - 1) * spacing
        let finalWidth: CGFloat = (width - totalSpacing) / itemsInRow
        return finalWidth - 25.0
    }
}
