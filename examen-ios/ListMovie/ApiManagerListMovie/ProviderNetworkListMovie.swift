//
//  ProviderNetworkListMovie.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation

class ProviderNetworkListMovie {
    static let shared: ProviderNetworkListMovie = ProviderNetworkListMovie()
    func callListMovie(completion: @escaping (responseData) -> Void) -> Void {
        
        NetworkListMovie.shared.callListMovie(serviceURL: "/3/tv/airing_today?api_key=\(ApiKey.data)&language=en-US&page=1", httpMethod: .get) { (data, response, error) in
            completion((data, response, error))
        }
    }
}
