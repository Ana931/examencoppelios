//
//  AutoLayout.swift
//  examen-ios
//
//  Created by User on 17/09/22.
//

import Foundation
import UIKit

extension UIView {
    
    func setAnchors(topAnchor: NSLayoutAnchor<NSLayoutYAxisAnchor>? = nil, bottomAnchor: NSLayoutAnchor<NSLayoutYAxisAnchor>? = nil, leadingAnchor: NSLayoutAnchor<NSLayoutXAxisAnchor>? = nil, trailingAnchor: NSLayoutAnchor<NSLayoutXAxisAnchor>? = nil, topConstant: CGFloat = 0, bottomConstant: CGFloat = 0, leadingConstant: CGFloat = 0, trailingConstant: CGFloat = 0) {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let topAnchor: NSLayoutAnchor<NSLayoutYAxisAnchor> = topAnchor {
            self.topAnchor.constraint(equalTo: topAnchor, constant: topConstant).isActive = true
        }
        if let bottomAnchor: NSLayoutAnchor<NSLayoutYAxisAnchor> = bottomAnchor {
            self.bottomAnchor.constraint(equalTo: bottomAnchor, constant: bottomConstant).isActive = true
        }
        if let leadingAnchor: NSLayoutAnchor<NSLayoutXAxisAnchor> = leadingAnchor {
            self.leadingAnchor.constraint(equalTo: leadingAnchor, constant: leadingConstant).isActive = true
        }
        
        if let trailingAnchor: NSLayoutAnchor<NSLayoutXAxisAnchor> = trailingAnchor {
            self.trailingAnchor.constraint(equalTo: trailingAnchor, constant: trailingConstant).isActive = true
        }
    }
    
    func setHeight(heightConstant: CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(equalTo: self.heightAnchor, constant: heightConstant).isActive = true
    }
    
    func setWidth(widthConstant: CGFloat) {
        self.widthAnchor.constraint(equalTo: self.widthAnchor, constant: widthConstant).isActive = true
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
}
